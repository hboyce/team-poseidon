Poseidon
========

Developed for CSE182 Final Project - Fall 2014 UCSD
---------------------------------------------------

Poseidon Team
-------------
Hunter Boyce

Jasmine Nguyen

Katerina Simunkova

Nicole Widmont

Usage
-----
./annotator.py -h

Generate Data
------------
###pip install -r requirements.txt

###run pre_annotator.py to store local hg.19 copy and rsid list
####Download omim.txt to omim/ directory. http://www.omim.org/downloads

###./annotator.py -f <vcf_file>
####use -v for verbose mode

#####After completion, mutations.json and snpedia.json will be written to the script directory.

Viewing Data
------------
###python poseidon.py

###Upload .json files to form for viewing

###Poseidon is also hosted at https://powerful-fjord-6424.herokuapp.com/
