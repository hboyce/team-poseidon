import json
import os
from collections import OrderedDict
from flask import Flask, url_for, render_template, request, send_from_directory
from werkzeug import secure_filename

UPLOAD_FOLDER = '/tmp'
ALLOWED_EXTENSIONS = set(['json'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/', methods=['POST', 'GET'])
def index():
    error = None
    if request.method == 'POST':
        json_file = request.files['file']
        if json_file and allowed_file(json_file.filename):
            filename = secure_filename(json_file.filename)
            json_file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            f = open(os.path.join(app.config['UPLOAD_FOLDER'], filename), "rb")
        try:
            annotated_file = json.JSONDecoder(object_pairs_hook=OrderedDict).decode("\n".join(f.readlines()))
            keys = annotated_file.keys()
            keys.remove("Sex")
            keys.remove("Desc")
            f.close()
            return render_template('view.html', af=annotated_file, keys=keys)
        except(ValueError):
            error = 'Error processing the file. Please resubmit.'
    return render_template('index.html', error=error)


@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == '__main__':
    app.run(debug=True)
